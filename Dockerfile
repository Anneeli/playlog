FROM node:16 as build-stage
RUN apt update -y && apt upgrade -y

WORKDIR /app
COPY package.json ./
RUN yarn install
COPY . /app

ENV SERVER_HOST=0.0.0.0
ENV SERVER_PORT=9080
RUN APP_URL=https://dev.playlog.com
RUN API_BASE_URL="https://dev.playlog.com/api/"

## this will help to get dist folder which we were copying in nginx #####

RUN yarn generate 


### Stage 2 ############

FROM nginx
ENV SERVER_HOST=0.0.0.0
ENV SERVER_PORT=9080
RUN APP_URL=https://dev.playlog.com
RUN API_BASE_URL="https://dev.playlog.com/api/"
COPY ./_docker/nginx/templates/ /root/etc/nginx/conf.d/default.conf
WORKDIR /var/www/html
COPY --from=build-stage /app/dist/ /var/www/html
EXPOSE 9080
CMD ["nginx", "-g", "daemon off;"]
